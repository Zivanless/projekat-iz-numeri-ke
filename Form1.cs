﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
namespace NumProj
{
    public partial class Form1 : Form
    {
        enum Tools {
            Hand = 0,
            Draw, Pixel
        }
        Pen p;
        Tools stanje;
        bool mousedown;
        double x, y;
        int xd, yd;
        Screen screen;
        Graphics g;
        Funkcija[] F;
        Funkcija f;
        int max_funkcija;
        int br_funkcija;
        double xlow, ylow, xhigh, yhigh;
        Boolean b;
        public Form1()
        {
            p = new Pen(Color.Black);
            InitializeComponent();
            stanje = Tools.Hand;
            mousedown = false;
            DoubleBuffered = true;
            screen = new Screen(pictureBox1.Width,pictureBox1.Height);
            handButton.BackColor = Color.LightBlue;
            br_funkcija = 0;
            max_funkcija = 16; //Odgovor na pitanje univerzuma, postojanja i svega
            Opacity = 0.94;
            f = null;
            F = new Funkcija[max_funkcija];
            xlow = ylow = xhigh = yhigh = 0;
            b = false;
        }

        private void picturebox1_MouseWheel(object sender, MouseEventArgs e) 
        {
            int y = e.Y - pictureBox1.Top - groupBox1.Top;
            int x = e.X - pictureBox1.Left - groupBox1.Left;
            if(stanje == Tools.Hand && x>=0 && y>=0 && x<=pictureBox1.Width && y<=pictureBox1.Height)
                screen.zoom(x,y,e.Delta);
            Refresh();
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            mousedown = true;
            x = xd = e.X;
            y = yd = e.Y;
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if(mousedown == true)
            {
                if(stanje == Tools.Hand)
                {
                    screen.translate(e.X - x, e.Y - y);
                }
                if(stanje == Tools.Draw)
                {
                    //dodaj e.x, e.y u tabelu, ali samo iza maksimuma, ili pre minimuma
                    if (f != null)
                    {
                        if (e.X < screen.x_to_screen(f.MIN()) || e.X > screen.x_to_screen(f.MAX()))
                        {
                            f.dodaj(new Par(screen.y_from_screen(e.Y), screen.x_from_screen(e.X)));
                        }
                    }
                }
                if (stanje == Tools.Pixel)
                {
                    //dodaj e.x, e.y u tabelu, bilo gde
                }
            }
            x = e.X;
            y = e.Y;
            Refresh();
            toolStripStatusLabel1.Text = String.Format("({0}, {1})", screen.x_text(e.X), screen.y_text(e.Y));
            //recrtaj
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            mousedown = false;
        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            if (stanje == Tools.Pixel && xd == e.X && yd == e.Y)
            {
                //dodaj jednu tacku u tabelu
                if(f!=null)
                    f.dodaj(new Par(screen.y_from_screen(e.Y), screen.x_from_screen(e.X)));
            }
        }

        private void resetintegralbounds()
        {
            xlow = ylow = xhigh = yhigh = 0;
            b = false;
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            g = e.Graphics;
            //g.Clear(Color.White);
            int d = 10;
            label1.Text = screen.right();
            label2.Text = screen.left();
            label3.Text = screen.up();
            label4.Text = screen.down();
            g.DrawLine(p, 0, (int)screen.y_to_screen(0), (int)screen.xmax(), (int)screen.y_to_screen(0));
            g.DrawLine(p, (int)screen.xmax() - d, (int)screen.y_to_screen(0) - d/2, (int)screen.xmax(), (int)screen.y_to_screen(0));
            g.DrawLine(p, (int)screen.xmax() - d, (int)screen.y_to_screen(0) + d/2, (int)screen.xmax(), (int)screen.y_to_screen(0));

            g.DrawLine(p, (int)screen.x_to_screen(0), 0, (int)screen.x_to_screen(0), (int)screen.ymax());
            g.DrawLine(p, (int)screen.x_to_screen(0) - d / 2, d, (int)screen.x_to_screen(0), 0);
            g.DrawLine(p, (int)screen.x_to_screen(0) + d / 2, d, (int)screen.x_to_screen(0), 0);

            g.DrawLine(p, (int)screen.x_to_screen(xlow), (int)screen.y_to_screen(0), (int)screen.x_to_screen(xlow), (int)screen.y_to_screen(ylow));
            g.DrawLine(p, (int)screen.x_to_screen(xhigh), (int)screen.y_to_screen(0), (int)screen.x_to_screen(xhigh), (int)screen.y_to_screen(yhigh));

            for (int i = 0; i < br_funkcija; i++)
            {
                F[i].crtaj(screen, g);
            }
            //List<Par> a = new List<Par>();
            //List<Par> k = new List<Par>();
            //a.Add(new Par(1, 2));
            //k.Add(new Par(1, -1));
            //k.Add(new Par(0, 0));
            //k.Add(new Par(1, 1));
            //Polinomska f = new Polinomska(a);
            //Tabelarna t = new Tabelarna(k);
            //t.Izvod().crtaj(screen, g);
        }

        private void liste()
        {
            listView1.Clear();
            for (int i = 0; i < br_funkcija; i++)
            {
                ListViewItem it = new ListViewItem(F[i].Ime);
                it.ForeColor = F[i].Boja;
                if (f == F[i])
                    it.BackColor = Color.FromArgb((128 + f.Boja.R) % 256, (128 + f.Boja.G) % 256, (128 + f.Boja.B) % 256);
                listView1.View = View.List;
                listView1.Items.Add(it);
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_MouseEnter(object sender, EventArgs e)
        {
            
        }

        private void pictureBox1_MouseLeave(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = "";
        }

        private void centerButton_Click(object sender, EventArgs e)
        {
            screen.resetcenter();
            Refresh();
        }

        private void scaleButton_Click(object sender, EventArgs e)
        {
            screen.reset();
            Refresh();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //treba da se ispisu komande input menija
            string s = "";
            s += "Naredbe:\n";
            s += "function <ime> x^7 + ... //pravi polinomsku funckiju sa zadatim imenom\n";
            s += "function <ime> draw //pravi tabelarnu funkciju\n";
            s += "integral <ime> interval low high // racuna odredjeni integral za funkciju na intervalu\n";
            s += "integral <ime> offset C //racuna neodredjeni integral sa offsetom C i daje mu naziv $<ime>\n";
            s += "derivative <ime> //pravi funkciju izvoda i daje ime <ime>'\n";
            s += "interpolate <ime> <ime2> // pravi interpolacioni polinom za f-ju <ime> i daje mu naziv <ime2>\n";
            s += "invisible <ime> //stavlja vidljivost na false\n";
            s += "visible <ime> //stavlja vidljivost na true\n";
            s += "color <ime> red green blue // stavlja boju na odgovorajuci kod u rgb formatu\n";
            s += "add <ime> <ime2> // pravi funckiju \"(<ime> + <ime2>)\"\n";
            s += "sub <ime> <ime2>// pravi funckiju \"(<ime> - <ime2>)\"\n";
            MessageBox.Show(s);
        }

        private void handButton_Click(object sender, EventArgs e)
        {
            stanje = Tools.Hand;
            handButton.BackColor = Color.LightBlue;
            drawButton.BackColor = Color.Transparent;
            pixelButton.BackColor = Color.Transparent;
        }

        private void drawButton_Click(object sender, EventArgs e)
        {
            stanje = Tools.Draw;
            drawButton.BackColor = Color.LightBlue;
            handButton.BackColor = Color.Transparent;
            pixelButton.BackColor = Color.Transparent;
        }

        private void pixelButton_Click(object sender, EventArgs e)
        {
            stanje = Tools.Pixel;
            pixelButton.BackColor = Color.LightBlue;
            drawButton.BackColor = Color.Transparent;
            handButton.BackColor = Color.Transparent;
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            string s;
            if (e.KeyChar != Convert.ToChar(Keys.Return))
                return;
            s = textBox1.Text;
            textBox1.Text = "";
            label6.Text = parsiraj_i_izvrsi(s);
            Refresh();
            listView1.Clear();
            liste();
        }

        private void doButton_Click(object sender, EventArgs e)
        {
            string s = textBox1.Text;
            textBox1.Text = "";
            label6.Text = parsiraj_i_izvrsi(s);
            Refresh();
            listView1.Clear();
            liste();
        }

        private int index_funkcije(string ime)
        {
            for (int i = 0; i < br_funkcija; i++)
            {
                if (F[i].Ime == ime)
                    return i;
            }
            return -1;
        }

        private string parsiraj_i_izvrsi(string s)//vraca status
        {
            char[] delimiterChars = {' '};
            string[] words = s.Split(delimiterChars);
            int i = 0;
            string action = "";
            string name1 = "";
            if (i == words.Length)
                return "Unos se ne prihvata! ";
            while (words[i] == "" && i<words.Length)
                i++;
            if(i == words.Length)
                return "Unos se ne prihvata! ";
            action = words[i++];
            if (i == words.Length)
                return "Unos se ne prihvata! ";
            while (words[i] == "" && i < words.Length)
                i++;
            if (i == words.Length)
                return "Unos se ne prihvata! ";
            name1 = words[i++];
            if (action == "invisible")
            {
                int a = index_funkcije(name1);
                if (a != -1)
                {
                    F[a].Vidljivost = false;
                    return "";
                }
                ;//set name1 to invisible
                return "Funkcija sa datim imenom ne postoji! ";
            }
            if (action == "visible")
            {
                int a = index_funkcije(name1);
                if (a != -1)
                {
                    F[a].Vidljivost = true;
                    return "";
                }
                ;//set name1 to visible
                return "Funkcija sa datim imenom ne postoji! ";
            }
            if (action == "color")
            {
                if (i == words.Length)
                    return "Unos se ne prihvata! ";
                while (words[i] == "" && i < words.Length)
                    i++;
                if (i == words.Length)
                    return "Unos se ne prihvata! ";
                string red = words[i++];
                while (words[i] == "" && i < words.Length)
                    i++;
                if (i == words.Length)
                    return "Unos se ne prihvata! ";
                string green = words[i++];
                while (words[i] == "" && i < words.Length)
                    i++;
                if (i == words.Length)
                    return "Unos se ne prihvata! ";
                string blue = words[i++];
                try
                {
                    int a = index_funkcije(name1);
                    if (a != -1)
                    {
                        F[a].Boja = Color.FromArgb(Convert.ToInt32(red), Convert.ToInt32(green), Convert.ToInt32(blue));
                        return "";
                    }
                    return "Funkcija sa datim imenom ne postoji! ";
                    //Color.FromArgb(Convert.ToInt32(red), Convert.ToInt32(green), Convert.ToInt32(blue));
                    //set color name1 to 
                }
                catch 
                {
                    return "Unos se ne prihvata! ";
                }
                return "";
            }
            if (action == "add")
            {
                if (i == words.Length)
                    return "Unos se ne prihvata! ";
                while (words[i] == "" && i < words.Length)
                    i++;
                if (i == words.Length)
                    return "Unos se ne prihvata! ";
                string name2 = words[i++];
                int a = index_funkcije(name1);
                int b = index_funkcije(name2);
                if (a == -1 || b == -1)
                {
                    return "Funkcija sa datim imenom ne postoji! ";
                }
                if(br_funkcija == max_funkcija)
                    return "Imate maksimalni broj funkcija! ";

                f = F[br_funkcija++] = F[a] + F[b];
                f.Ime = "(" + name1 + "+" + name2 + ")";
                //nova funckija ime = "(name1 + name2)" i vred add(name1, name2)
                return "";
            }
            if (action == "sub")
            {
                if (i == words.Length)
                    return "Unos se ne prihvata! ";
                while (words[i] == "" && i < words.Length)
                    i++;
                if (i == words.Length)
                    return "Unos se ne prihvata! ";
                string name2 = words[i++];
                int a = index_funkcije(name1);
                int b = index_funkcije(name2);
                if (a == -1 || b == -1)
                {
                    return "Funkcija sa datim imenom ne postoji! ";
                }
                if (br_funkcija == max_funkcija)
                    return "Imate maksimalni broj funkcija! ";

                f = F[br_funkcija++] = F[a] - F[b];
                f.Ime = "(" + name1 + "-" + name2 + ")";
                //nova funckija ime = "(name1 - name2)" i vred sub(name1, name2)
                return "";
            }
            if (action == "derivative")
            {
                int a = index_funkcije(name1);
                if (a == -1)
                {
                    return "Funkcija sa datim imenom ne postoji! ";
                }
                if(br_funkcija == max_funkcija)
                    return "Imate maksimalni broj funkcija! ";
                f = F[br_funkcija++] = F[a].Izvod();
                f.Ime = F[a].Ime + "'";
                //f = F[a];
                //nova funkcija ime = name1+"'" i vred izvod(name1)
                return "";
            }
            if (action == "interpolate")
            {
                if (i == words.Length)
                    return "Unos se ne prihvata! ";
                while (words[i] == "" && i < words.Length)
                    i++;
                if (i == words.Length)
                    return "Unos se ne prihvata! ";
                string name2 = words[i++];
                int a = index_funkcije(name1);
                if (a == -1)
                {
                    return "Funkcija sa datim imenom ne postoji! ";
                }
                if (br_funkcija == max_funkcija)
                    return "Imate maksimalni broj funkcija! ";
                f = F[br_funkcija++] = F[a].Interpolacioni();
                if (f == F[a])
                {
                    br_funkcija--;
                    return "";
                }
                f.Ime = name2;

                //nova funckija ime = name2 i vred interol(name)
                return "";
            }
            if (action == "integral")
            {
                if (i == words.Length)
                    return "Unos se ne prihvata! ";
                while (words[i] == "" && i < words.Length)
                    i++;
                if (i == words.Length)
                    return "Unos se ne prihvata! ";
                string word2 = words[i++];
                if (word2 == "interval")
                {
                    if (i == words.Length)
                        return "Unos se ne prihvata! ";
                    while (words[i] == "" && i < words.Length)
                        i++;
                    if (i == words.Length)
                        return "Unos se ne prihvata! ";
                    string low = words[i++];
                    while (words[i] == "" && i < words.Length)
                        i++;
                    if (i == words.Length)
                        return "Unos se ne prihvata! ";
                    string high = words[i++];
                    try
                    {
                        int a = index_funkcije(name1);
                        if (a == -1)
                        {
                            return "Funkcija sa datim imenom ne postoji! ";
                        }
                        xlow = Math.Max(Double.Parse(low), F[a].MIN());
                        xhigh = Math.Min(Double.Parse(high), F[a].MAX());
                        ylow = F[a].fiktivna(xlow);
                        yhigh = F[a].fiktivna(xhigh);
                        b = true;
                        return F[a].integral(Double.Parse(low), Double.Parse(high));
                        //racuna rezlutat intergala i gresku i stavlja je gde treba
                        //Convert.ToInt32(low), Convert.ToInt32(high)
                    }
                    catch 
                    {
                        return "Unos se ne prihvata! ";
                    }
                    return "";
                }
                if (word2 == "offset")
                {
                    if (i == words.Length)
                        return "Unos se ne prihvata! ";
                    while (words[i] == "" && i < words.Length)
                        i++;
                    if (i == words.Length)
                        return "Unos se ne prihvata! ";
                    string C = words[i++];
                    try
                    {
                        int a = index_funkcije(name1);
                        if (a == -1)
                        {
                            return "Funkcija sa datim imenom ne postoji! ";
                        }
                        if (br_funkcija == max_funkcija)
                            return "Imate maksimalni broj funkcija! ";

                        f = F[br_funkcija++] = F[a].OpstiIntegral(Double.Parse(C));
                        f.Ime = "$" + name1;
                        //f = F[a];
                        return "";
                        //new function ime = $ + name1, vred integral(name1, Convert.ToInt32(C))
                    }
                    catch
                    {
                        return "Unos se ne prihvata! ";
                    }
                    
                }
                return "Unos se ne prihvata! ";
            }
            if (action == "function")
            {
                if (i == words.Length)
                    return "Unos se ne prihvata! ";
                while (words[i] == "" && i < words.Length)
                    i++;
                if (i == words.Length)
                    return "Unos se ne prihvata! ";
                string name2 = "";
                for (; i<words.Length; i++) 
                    name2 += words[i];
                int a = index_funkcije(name1);
                if (a != -1)
                    return "Funkcija sa tim imenom vec postoji! ";
                if (br_funkcija == max_funkcija)
                    return "Dostigli ste maksimalni broj funkcija! ";
                if (name2 == "draw")
                {
                    F[br_funkcija] = new Tabelarna(name1);
                    f = F[br_funkcija];
                    br_funkcija++;
                }
                else 
                {
                    if (a != -1)
                        return "Funkcija sa tim imenom vec postoji! ";
                    if (br_funkcija == max_funkcija)
                        return "Dostigli ste maksimalni broj funkcija! ";
                    try 
                    {
                        F[br_funkcija] = new Polinomska(name2, name1);
                        f = F[br_funkcija];
                        br_funkcija++;
                    }
                    catch 
                    {
                        return "Ne prihvata se unos! ";
                    }
                    //nova funckija ime = name1, vred new polinom()
                    //proverava sta se vraca, mozda je unos kurcina
                }
                return "";
            }
            if (action == "select")
            {
                int a = index_funkcije(name1);
                if(a!=-1)
                {
                    f = F[a];
                    return "";
                }
                return "Nema funkcije sa tim imenom! ";
            }
            return "Ne prihvata se unos! ";
            
        }

        private void groupBox5_Enter(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //Refresh();
        }

        private void listView1_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            f = F[e.ItemIndex];
            liste();
        }

        private string update(string s = "")
        {
            string sh = "";
            if (s != "")
                sh = parsiraj_i_izvrsi(s);
            liste();
            Refresh();
            return sh;
        }

        private void interpolDugme_Click(object sender, EventArgs e)
        {
            if(f != null)
                label6.Text = update("interpolate "+f.Ime +" "+f.Ime+"_intp");
        }

        private void izvodDugme_Click(object sender, EventArgs e)
        {
            if (f != null)
                label6.Text = update("derivative " + f.Ime);
        }

        private void neodintegralDugme_Click(object sender, EventArgs e)
        {
            if (f != null)
                label6.Text = update("integral " + f.Ime + " offset 0");
        }

        private void vidljivostDugme_Click(object sender, EventArgs e)
        {
            if (f != null)
                f.Vidljivost = !f.Vidljivost;
            update();
        }

        private void bojaDugme_Click(object sender, EventArgs e)
        {
            if (f == null)
                return;
            ColorDialog MyDialog = new ColorDialog();
            // Keeps the user from selecting a custom color.
            MyDialog.AllowFullOpen = false;
            // Allows the user to get help. (The default is false.)
            MyDialog.ShowHelp = true;
            // Sets the initial color select to the current text color.
            MyDialog.Color = f.Boja;

            // Update the text box color if the user clicks OK  
            if (MyDialog.ShowDialog() == DialogResult.OK)
                f.Boja = MyDialog.Color;
            update();
        }

        private void novafunkcijaDugme_Click(object sender, EventArgs e)
        {
            update("function fn"+Convert.ToString(br_funkcija+1)+ " draw");
        }

        private void pdrintegralDugme_Click(object sender, EventArgs e)
        {

        }

        private void label6_TextChanged(object sender, EventArgs e)
        {
            if (Text != "" && !b)
                resetintegralbounds();
            b = false;
            update();
        }

        /*private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.SuspendLayout();
            // 
            // Form1
            // 
            this.ClientSize = new System.Drawing.Size(284, 262);
            
            this.Name = "Form1";
            this.ResumeLayout(false);

        }*/

    }
}
