﻿namespace NumProj
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Drawing.Icon ico = new System.Drawing.Icon("test.ico");
            this.Icon = ico;
            this.components = new System.ComponentModel.Container();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.pixelButton = new System.Windows.Forms.Button();
            this.scaleButton = new System.Windows.Forms.Button();
            this.drawButton = new System.Windows.Forms.Button();
            this.centerButton = new System.Windows.Forms.Button();
            this.handButton = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.novafunkcijaDugme = new System.Windows.Forms.Button();
            this.bojaDugme = new System.Windows.Forms.Button();
            this.vidljivostDugme = new System.Windows.Forms.Button();
            this.neodintegralDugme = new System.Windows.Forms.Button();
            this.izvodDugme = new System.Windows.Forms.Button();
            this.interpolDugme = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.listView1 = new System.Windows.Forms.ListView();
            this.doButton = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(6, 14);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(831, 673);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
            this.pictureBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pictureBox1.MouseEnter += new System.EventHandler(this.pictureBox1_MouseEnter);
            this.pictureBox1.MouseLeave += new System.EventHandler(this.pictureBox1_MouseLeave);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            this.pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Location = new System.Drawing.Point(62, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(843, 695);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(854, 710);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "label1";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(65, 710);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "label2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "label3";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 686);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "label4";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 771);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1112, 22);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 5;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(6, 736);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(819, 20);
            this.textBox1.TabIndex = 6;
            this.textBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Controls.Add(this.pixelButton);
            this.groupBox2.Controls.Add(this.scaleButton);
            this.groupBox2.Controls.Add(this.drawButton);
            this.groupBox2.Controls.Add(this.centerButton);
            this.groupBox2.Controls.Add(this.handButton);
            this.groupBox2.Location = new System.Drawing.Point(911, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(189, 164);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Alati";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(106, 115);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(77, 42);
            this.button2.TabIndex = 2;
            this.button2.Text = "Komande";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // pixelButton
            // 
            this.pixelButton.BackColor = System.Drawing.Color.Transparent;
            this.pixelButton.Location = new System.Drawing.Point(7, 115);
            this.pixelButton.Name = "pixelButton";
            this.pixelButton.Size = new System.Drawing.Size(82, 42);
            this.pixelButton.TabIndex = 2;
            this.pixelButton.Text = "Tacka po tacka";
            this.pixelButton.UseVisualStyleBackColor = false;
            this.pixelButton.Click += new System.EventHandler(this.pixelButton_Click);
            // 
            // scaleButton
            // 
            this.scaleButton.Location = new System.Drawing.Point(106, 67);
            this.scaleButton.Name = "scaleButton";
            this.scaleButton.Size = new System.Drawing.Size(77, 42);
            this.scaleButton.TabIndex = 1;
            this.scaleButton.Text = "Vrati polozaj i skaliranost";
            this.scaleButton.UseVisualStyleBackColor = true;
            this.scaleButton.Click += new System.EventHandler(this.scaleButton_Click);
            // 
            // drawButton
            // 
            this.drawButton.Location = new System.Drawing.Point(7, 67);
            this.drawButton.Name = "drawButton";
            this.drawButton.Size = new System.Drawing.Size(82, 42);
            this.drawButton.TabIndex = 1;
            this.drawButton.Text = "Crtanje";
            this.drawButton.UseVisualStyleBackColor = true;
            this.drawButton.Click += new System.EventHandler(this.drawButton_Click);
            // 
            // centerButton
            // 
            this.centerButton.Location = new System.Drawing.Point(106, 19);
            this.centerButton.Name = "centerButton";
            this.centerButton.Size = new System.Drawing.Size(77, 42);
            this.centerButton.TabIndex = 0;
            this.centerButton.Text = "Vrati na centar";
            this.centerButton.UseVisualStyleBackColor = true;
            this.centerButton.Click += new System.EventHandler(this.centerButton_Click);
            // 
            // handButton
            // 
            this.handButton.Location = new System.Drawing.Point(7, 19);
            this.handButton.Name = "handButton";
            this.handButton.Size = new System.Drawing.Size(82, 42);
            this.handButton.TabIndex = 0;
            this.handButton.Text = "Pomeranje";
            this.handButton.UseVisualStyleBackColor = true;
            this.handButton.Click += new System.EventHandler(this.handButton_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.novafunkcijaDugme);
            this.groupBox3.Controls.Add(this.bojaDugme);
            this.groupBox3.Controls.Add(this.vidljivostDugme);
            this.groupBox3.Controls.Add(this.neodintegralDugme);
            this.groupBox3.Controls.Add(this.izvodDugme);
            this.groupBox3.Controls.Add(this.interpolDugme);
            this.groupBox3.Location = new System.Drawing.Point(911, 182);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(189, 165);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Dodatne opcije";
            // 
            // novafunkcijaDugme
            // 
            this.novafunkcijaDugme.Location = new System.Drawing.Point(106, 19);
            this.novafunkcijaDugme.Name = "novafunkcijaDugme";
            this.novafunkcijaDugme.Size = new System.Drawing.Size(77, 42);
            this.novafunkcijaDugme.TabIndex = 6;
            this.novafunkcijaDugme.Text = "Nova funkcija";
            this.novafunkcijaDugme.UseVisualStyleBackColor = true;
            this.novafunkcijaDugme.Click += new System.EventHandler(this.novafunkcijaDugme_Click);
            // 
            // bojaDugme
            // 
            this.bojaDugme.Location = new System.Drawing.Point(106, 115);
            this.bojaDugme.Name = "bojaDugme";
            this.bojaDugme.Size = new System.Drawing.Size(77, 42);
            this.bojaDugme.TabIndex = 5;
            this.bojaDugme.Text = "Promeni boju";
            this.bojaDugme.UseVisualStyleBackColor = true;
            this.bojaDugme.Click += new System.EventHandler(this.bojaDugme_Click);
            // 
            // vidljivostDugme
            // 
            this.vidljivostDugme.Location = new System.Drawing.Point(106, 67);
            this.vidljivostDugme.Name = "vidljivostDugme";
            this.vidljivostDugme.Size = new System.Drawing.Size(77, 42);
            this.vidljivostDugme.TabIndex = 4;
            this.vidljivostDugme.Text = "Promeni vidljivost";
            this.vidljivostDugme.UseVisualStyleBackColor = true;
            this.vidljivostDugme.Click += new System.EventHandler(this.vidljivostDugme_Click);
            // 
            // neodintegralDugme
            // 
            this.neodintegralDugme.Location = new System.Drawing.Point(7, 115);
            this.neodintegralDugme.Name = "neodintegralDugme";
            this.neodintegralDugme.Size = new System.Drawing.Size(82, 42);
            this.neodintegralDugme.TabIndex = 2;
            this.neodintegralDugme.Text = "Neodredjeni integral";
            this.neodintegralDugme.UseVisualStyleBackColor = true;
            this.neodintegralDugme.Click += new System.EventHandler(this.neodintegralDugme_Click);
            // 
            // izvodDugme
            // 
            this.izvodDugme.Location = new System.Drawing.Point(7, 67);
            this.izvodDugme.Name = "izvodDugme";
            this.izvodDugme.Size = new System.Drawing.Size(82, 42);
            this.izvodDugme.TabIndex = 1;
            this.izvodDugme.Text = "Izvod";
            this.izvodDugme.UseVisualStyleBackColor = true;
            this.izvodDugme.Click += new System.EventHandler(this.izvodDugme_Click);
            // 
            // interpolDugme
            // 
            this.interpolDugme.Location = new System.Drawing.Point(7, 19);
            this.interpolDugme.Name = "interpolDugme";
            this.interpolDugme.Size = new System.Drawing.Size(82, 42);
            this.interpolDugme.TabIndex = 0;
            this.interpolDugme.Text = "Interpoliraj";
            this.interpolDugme.UseVisualStyleBackColor = true;
            this.interpolDugme.Click += new System.EventHandler(this.interpolDugme_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.listView1);
            this.groupBox4.Location = new System.Drawing.Point(912, 353);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(188, 246);
            this.groupBox4.TabIndex = 10;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Funkcije";
            // 
            // listView1
            // 
            this.listView1.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listView1.Location = new System.Drawing.Point(6, 19);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(176, 221);
            this.listView1.TabIndex = 1;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.listView1_ItemSelectionChanged);
            // 
            // doButton
            // 
            this.doButton.Location = new System.Drawing.Point(831, 733);
            this.doButton.Name = "doButton";
            this.doButton.Size = new System.Drawing.Size(75, 23);
            this.doButton.TabIndex = 11;
            this.doButton.Text = "Do!";
            this.doButton.UseVisualStyleBackColor = true;
            this.doButton.Click += new System.EventHandler(this.doButton_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Location = new System.Drawing.Point(912, 605);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(188, 102);
            this.groupBox5.TabIndex = 12;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Status";
            this.groupBox5.Enter += new System.EventHandler(this.groupBox5_Enter);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(24, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 13);
            this.label6.TabIndex = 1;
            this.label6.TextChanged += new System.EventHandler(this.label6_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 13);
            this.label5.TabIndex = 0;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 30;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1112, 793);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.doButton);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.HelpButton = true;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Interpol";
            this.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.picturebox1_MouseWheel);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button doButton;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button scaleButton;
        private System.Windows.Forms.Button centerButton;
        private System.Windows.Forms.Button handButton;
        private System.Windows.Forms.Button pixelButton;
        private System.Windows.Forms.Button drawButton;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button interpolDugme;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button neodintegralDugme;
        private System.Windows.Forms.Button izvodDugme;
        private System.Windows.Forms.Button novafunkcijaDugme;
        private System.Windows.Forms.Button bojaDugme;
        private System.Windows.Forms.Button vidljivostDugme;
    }
}

