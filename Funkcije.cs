﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Text.RegularExpressions;
namespace NumProj
{

    //KOMPARATOR
    public class Poredi_polinome : IComparer<Par>
    {
        public int Compare(Par x, Par y)
        {
            int compareDate = y.b.CompareTo(x.b);
            return compareDate;
        }
    }

    public class Poredi_tabelu : IComparer<Par>
    {
        public int Compare(Par x, Par y)
        {
            int compareDate = x.b.CompareTo(y.b);
            return compareDate;
        }
    }



    public struct Par
    {
        public double a, b;

        /*public Par()
        {
            a = b = 0;
        }*/
        public Par(double q, double w)
        {
            a = q;
            b = w;
        }
        public static bool operator ==(Par p1, Par p2) { return (p1.a == p2.a && p1.b == p2.b); }
        public static bool operator !=(Par p1, Par p2) { return !(p1 == p2); }

    }



    public abstract class Funkcija
    {
        string ime;
        public abstract Polinomska Interpolacioni();
        public abstract Funkcija OpstiIntegral(double C = 0);
        public abstract Funkcija Izvod();
        public abstract double OdredjeniIntegral(double a1, double a2);
        public abstract double IntegralUTacki(double t, double C = 0);
        public abstract double Vrednost(double X);
        public abstract void crtaj(Screen S, System.Drawing.Graphics G);
        public virtual void dodaj(Par w) { }
        public abstract double greska_int();

        public Funkcija()
        {
            Random r = new Random();
            boja = Color.FromArgb(r.Next(256), r.Next(256), r.Next(256));
        }

        //DODATI
        protected virtual Funkcija N_izvod(int n)
        {
            Funkcija p = Izvod();
            for (int i = 1; i < n; i++)
            {
                p = p.Izvod();
            }
            return p;
        }

        //DODAJ
        public virtual string integral(double a1, double a2)
        {
            double pomoc = OdredjeniIntegral(a1, a2);
            double greska = greska_int();
            string s = pomoc.ToString("N" + 4) + " ± " + greska.ToString("N" + 4);
            return s;
        }
        public virtual double MIN() { return -Math.Pow(10, 60); }


        public static Polinomska operator +(Funkcija p1, Funkcija p2)
        {
            return Polinomska.saberi(p1.Interpolacioni(), p2.Interpolacioni());
        }
        public static Polinomska operator -(Funkcija p1, Funkcija p2)
        {
            return Polinomska.oduzmi(p1.Interpolacioni(), p2.Interpolacioni());
        }

        public virtual double MAX() { return Math.Pow(10, 60); }

        public virtual double fiktivna(double x)
        { return Vrednost(x); }


        protected static int ID = 1;

        Boolean vidljivost = true;
        System.Drawing.Color boja;
        public string Ime
        {
            get
            { return ime; }
            set
            { ime = value; }
        }

        public Boolean Vidljivost
        {
            get
            { return vidljivost; }
            set
            { vidljivost = value; }
        }

        public System.Drawing.Color Boja
        {
            get
            { return boja; }
            set
            { boja = value; }
        }


    }



    public class Polinomska : Funkcija
    {
        List<Par> Koef;

        static IComparer<Par> comparer = new Poredi_polinome();

        private void dodaj(Par e)
        {
            foreach (Par q in Koef)
                if (q.b == e.b)
                {
                    e.a += q.a;
                    Koef.Remove(q);
                    break;
                }
            Koef.Add(e);
            Koef.Sort(comparer);
        }


        public override Polinomska Interpolacioni()
        {
            return this;
        }

        public Polinomska(string s = "")
        {
            Koef = new List<Par>(0);
            if (s == "")
                Ime = "F_" + (ID++).ToString();
            else
                Ime = s;
        }

        public Polinomska(Par[] w, int size, string s = "")
        {
            Koef = new List<Par>(size);
            for (int i = 0; i < size; i++)
                dodaj(w[i]);
            Koef.Sort(comparer);
            if (s == "")
                Ime = "F_" + (ID++).ToString();
            else
                Ime = s;
        }

        public Polinomska(List<Par> w, string s = "")
        {

            Koef = new List<Par>(w.Capacity);
            foreach (Par q in w)
                dodaj(q);
            Koef.Sort(comparer);
            Ime = "F_" + (ID++).ToString();
            if (s == "")
                Ime = "F_" + (ID++).ToString();
            else
                Ime = s;
        }

        public Polinomska(Polinomska w)
        {
            Koef = new List<Par>(w.Koef.Capacity);
            foreach (Par q in w.Koef)
                dodaj(q);
            Koef.Sort(comparer);
            Ime = w.Ime;
        }

        public override double Vrednost(double X)
        {
            double pomoc = 0;
            foreach (Par q in Koef)
            {
                pomoc += q.a * Math.Pow(X, q.b);
            }
            return pomoc;
        }

        public override Funkcija OpstiIntegral(double C = 0)
        {
            if (Koef.Capacity == 0) return this;

            List<Par> nova = new List<Par>(Koef.Capacity + 1);

            foreach (Par q in Koef)
            {
                Par w;
                w.a = q.a / (q.b + 1);
                w.b = q.b + 1;
                nova.Add(w);
            }
            if (C != 0) nova.Add(new Par(C, 0));


            Polinomska p = new Polinomska(nova);
            return p;
        }

        public override double IntegralUTacki(double t, double C = 0)
        {
            Funkcija pom = OpstiIntegral(C);
            return pom.Vrednost(t);
            //Polinomska integral = new Polinomska(this.OpstiIntegral(C));
            //return integral.Vrednost(t);
        }

        public override double OdredjeniIntegral(double a1, double a2)
        {
            double a = IntegralUTacki(a2);
            double b = IntegralUTacki(a1);
            double rez = a - b;
            return rez;
        }

        public override Funkcija Izvod()
        {
            if (Koef.Capacity == 0) return this;

            List<Par> nova = new List<Par>(Koef.Capacity);

            foreach (Par q in Koef)
            {
                Par w;
                w.a = q.a * q.b;
                w.b = q.b - 1;
                if (w.b >= 0)
                    nova.Add(w);
            }

            Polinomska p = new Polinomska(nova);
            return p;
        }

        public string ToString(int n = 2)
        {
            String s = "";
            int i = 0;
            if (Koef.Capacity == 0) return "0";
            else
                foreach (Par w in Koef)
                    if (i++ == 0) s = s + w.a.ToString("N" + n) + "*x ^" + ((int)w.b).ToString("N" + 0);
                    else
                        if (w.b != 0)
                            s = w.a != 0 ? s + " + " + w.a.ToString("N" + n) + "*x ^" + ((int)w.b).ToString("N" + 0) : s;
                        else
                            s = w.a != 0 ? s + " + " + w.a.ToString("N" + n) : s;
            return s;
        }

        public static Polinomska operator *(Polinomska p1, Polinomska p2)
        {
            List<Par> pom = new List<Par>(0);

            int stepen = 1;
            foreach (Par q in p1.Koef)
            {
                stepen *= (int)q.b;
                break;
            }
            foreach (Par q in p2.Koef)
            {
                stepen *= (int)q.b;
                break;
            }
            stepen += 2;


            double[] Niz = new double[stepen];

            for (int i = 0; i < stepen; i++)
                Niz[i] = 0;

            foreach (Par w in p1.Koef)
                foreach (Par q in p2.Koef)
                    Niz[(int)(q.b + w.b)] += q.a * w.a;

            for (int i = 0; i < stepen; i++)
                if (Niz[i] != 0) pom.Add(new Par(Niz[i], i));
            Polinomska rez = new Polinomska(pom);
            return rez;
        }

        public static Polinomska operator *(Polinomska p1, double p2)
        {
            List<Par> nova = new List<Par>(p1.Koef.Capacity);

            foreach (Par w in p1.Koef)
                nova.Add(new Par(w.a * p2, w.b));
            return new Polinomska(nova);
        }

        public static Polinomska saberi(Polinomska p1, Polinomska p2)
        {
            Polinomska rezultujuci = new Polinomska(p1);
            foreach (Par q2 in p2.Koef)
            {
                bool nesadrzi = true;
                foreach (Par q1 in rezultujuci.Koef)
                    if (q1.b == q2.b)
                    {
                        Par pom = new Par(q1.a + q2.a, q1.b);
                        rezultujuci.Koef.Remove(q1);
                        rezultujuci.dodaj(pom);
                        rezultujuci.Koef.Sort(comparer);
                        nesadrzi = false;
                        break;
                    }
                if (nesadrzi)
                {
                    rezultujuci.dodaj(q2);
                }
            }
            return rezultujuci;
        }

        public static Polinomska oduzmi(Polinomska p1, Polinomska p2)
        {
            List<Par> pom = new List<Par>(0);
            foreach (Par q in p2.Koef)
                pom.Add(new Par(-q.a, q.b));
            return saberi(p1, new Polinomska(pom));
        }

        public override void crtaj(Screen S, System.Drawing.Graphics G)
        {
            System.Drawing.Pen olovka = new System.Drawing.Pen(Boja);
            int BROJ_TACAKA = 500;
            System.Drawing.PointF[] niz = new System.Drawing.PointF[BROJ_TACAKA];
            double xmin = S.x_from_screen(0);
            double xmax = S.x_from_screen(S.xmax());
            double step = (xmax - xmin) / (BROJ_TACAKA - 1);
            for (int i = 0; i < BROJ_TACAKA; i++)
            {
                double x_coord = xmin + i * step;
                double y_coord = Vrednost(x_coord);
                /*if (Math.Abs(y_coord) > 100000)
                {
                    x_coord = xmin + (i -1) * step;
                    y_coord = Vrednost(x_coord);
                }*/
                x_coord = S.x_to_screen(x_coord);
                y_coord = S.y_to_screen(y_coord);
                if (x_coord > 10000000)
                    x_coord = 10000000;
                if (x_coord < -10000000)
                    x_coord = -10000000;
                if (y_coord > 10000000)
                    y_coord = 10000000;
                if (y_coord < -10000000)
                    y_coord = -10000000;
                niz[i] = new System.Drawing.PointF((float)x_coord, (float)y_coord);
            }
            if(Vidljivost)
                G.DrawLines(olovka, niz);
        }

        public override double greska_int()
        {
            return 0;
        }

        static public List<Par> parsiraj(string s)
        {
            string check = "";
            string temp = Regex.Replace(s, @"\s+", "");
            temp = temp.Replace("x+", "x^1+");
            temp = temp.Replace("x-", "x^1-");
            temp = temp.Replace("+x", "+1*x");
            temp = temp.Replace("-x", "-1*x");
            temp = Regex.Replace(temp, "x$", "x^1");
            temp = Regex.Replace(temp, "^x", "1*x");
            temp = Regex.Replace(temp, @"(?<broj>\d)x", "${broj}*x");
            temp = Regex.Replace(temp, @"(?<prvi>[^\d^.*])(?<koef>\d+(\.\d*)?)(?<drugi>[^\d.*]|$)", "${prvi}${koef}*x^0${drugi}");
            temp = Regex.Replace(temp, @"^(?<broj>[+|-]?\d+(?:\.\d*)?)(?<posle>[+-]|$)", "${broj}*x^0${posle}");

            List<Par> lista = new List<Par>();
            string expr = @"([+|-]?\d+(?:\.\d*)?)\*x\^(\d+(?:\.\d*)?)";
            MatchCollection hits = Regex.Matches(temp, expr);
            foreach (Match m in hits)
            {
                Par x = new Par(Double.Parse(m.Groups[1].Value), Double.Parse(m.Groups[2].Value));
                check += m.Groups[0];
                lista.Add(x);
            }

            if (check != temp)
                throw new System.ArgumentException("Nekorektan unos polinoma!!!");

            return lista;
        }

        public Polinomska(string pars, string s = ""): this(parsiraj(pars),s)
        {

        }

        

    }


    public class Tabelarna : Funkcija
    {
        List<Par> Broj;

        static IComparer<Par> compar = new Poredi_tabelu();


        public Tabelarna(string s = "")
        {
            Broj = new List<Par>(0);
            if (s == "")
                Ime = "F_" + (ID++).ToString();
            else
                Ime = s;
        }

        public Tabelarna(List<Par> w, string s = "")
        {

            Broj = new List<Par>(w.Capacity);
            foreach (Par q in w)
                Broj.Add(q);
            Broj.Sort(compar);
            if (s == "")
                Ime = "F_" + (ID++).ToString();
            else
                Ime = s;
        }

        public Tabelarna(Tabelarna w, string s = "")
        {
            Broj = new List<Par>(w.Broj.Capacity);
            foreach (Par q in w.Broj)
                Broj.Add(q);
            Broj.Sort(compar);
            Ime = "F_" + (ID++).ToString();
            if (s == "")
                Ime = "F_" + (ID++).ToString();
            else
                Ime = s;
        }

        //Pomocna funkcija za izracunavanje lagranzovog polinoma
        private Polinomska Pi(Par p)
        {
            if (!Broj.Contains(p)) return new Polinomska();

            List<Par> pom = new List<Par>(0);
            pom.Add(new Par(1, 0));

            Polinomska rez = new Polinomska(pom);


            double K = 1;
            foreach (Par q in Broj)
            {
                if (q != p)
                {
                    pom.Clear();
                    pom.Add(new Par(1, 1));
                    pom.Add(new Par(-q.b, 0));
                    rez = rez * new Polinomska(pom);
                    K *= (p.b - q.b);
                }
            }
            K = p.a / K;

            rez = rez * K;

            return rez;
        }

        public override Polinomska Interpolacioni()
        {
            Polinomska rez;

            rez = new Polinomska();

            foreach (Par w in Broj)
                rez = rez + this.Pi(w);

            return rez;
        }

        private double h()
        {
            double prvi = 0, poslednji = 0;
            int k = 0;
            foreach (Par w in Broj)
            {
                k++;
                if (prvi == 0) prvi = w.b;
                /*if (w.b != 0)*/
                poslednji = w.b;
            }
            return (poslednji - prvi) / k;
        }


        /*public override Polinomska OpstiIntegral(double C = 0)
        {
            return Interpolacioni().OpstiIntegral(C);
        }*/

        public override Funkcija Izvod()
        {
            List<Par> Izvod = new List<Par>();
            for (int i = 1; i < Broj.Count(); i++)
            {
                Par w;
                w.b = Broj[i].b;
                w.a = (Broj[i].a - Broj[i - 1].a) / (Broj[i].b - Broj[i - 1].b);
                Izvod.Add(w);
            }

            return new Tabelarna(Izvod);
        }


        /* Vraca vrednost odredjenog integrala na segmentu [a1, a2].
         * Ako su a1 i/ili a2 izvan opsega najmanje i najvece vrednosti u tabeli,
         * zamenice ih sa najmanjom i/ili najvecom vrednoscu u tabeli, respektivno*/
        
        
        public List<Par> interval(double manji, double veci)
        {
            List<Par> rez = new List<Par>(0);
            if (Broj[0].b >= veci || Broj[Broj.Count - 1].b <= manji)
                return rez;
            Par tren;
            long l = 0, h = Broj.Capacity;
            int mid = (int)(l + h) / 2;

            tren = Broj[mid];
            while (tren.b != manji && h > l)
            {
                if (tren.b < manji)
                { l = mid + 1; mid = (int)(h + l) / 2; }
                else
                { h = mid - 1; mid = (int)(l + h) / 2; }
                tren = Broj[mid];
            }
            int donji = (int)mid;


            l = 0;
            h = Broj.Capacity;
            mid = (int)(l + h) / 2;

            tren = Broj[mid];
            while (tren.b != veci && h > l)
            {
                if (tren.b < veci)
                { l = mid + 1; mid = (int)(h + l) / 2; }
                else
                { h = mid - 1; mid = (int)(l + h) / 2; }
                tren = Broj[mid];
            }

            int gornji = (int)mid;
            for (int i = donji; i <= gornji; i++)
                rez.Add(Broj[i]);

            return rez;

        }

        //Vraca vrednost primitivne funkcije interpolacionog polinoma (sa konstantom C) u tacki t
        public override double IntegralUTacki(double t, double C = 0)
        {
            Polinomska pom = Interpolacioni();
            return pom.IntegralUTacki(t, C);
        }

        //Vraca vrednost intepolacionog polinoma u tacki X
        public override double Vrednost(double X)
        {
            Polinomska pom = Interpolacioni();
            return pom.Vrednost(X);
        }



        public override void crtaj(Screen S, System.Drawing.Graphics G)
        {
            int duzina = Broj.Count();
            System.Drawing.Pen olovka = new System.Drawing.Pen(Boja);
            System.Drawing.PointF[] niz = new System.Drawing.PointF[duzina];

            for (int i = 0; i < duzina; i++)
            {
                float x_coord = (float)S.x_to_screen(Broj[i].b);
                float y_coord = (float)S.y_to_screen(Broj[i].a);
                if (x_coord > 10000000)
                    x_coord = 10000000;
                if (x_coord < -10000000)
                    x_coord = -10000000;
                if (y_coord > 10000000)
                    y_coord = 10000000;
                if (y_coord < -10000000)
                    y_coord = -10000000;
                niz[i] = new System.Drawing.PointF(x_coord, y_coord);
            }
            if(duzina>1 && Vidljivost)
                G.DrawLines(olovka, niz);
        }
        public override void dodaj(Par w)
        {
            Broj.Add(w);
            Broj.Sort(compar);
        }

        public override double MIN()
        {
            if (Broj.Count == 0)
                return Math.Pow(10,60);
            return Broj[0].b;
        }

        public override double MAX()
        {
            if (Broj.Count == 0)
                return -Math.Pow(10, 60);
            return Broj[Broj.Count-1].b;
        }

        public Polinomska greska_interpol()
        {
            List<Par> pom = new List<Par>(0);
            pom.Add(new Par(1, 0));
            Polinomska rez = new Polinomska(pom);

            foreach (Par q in Broj)
            {

                pom.Clear();
                pom.Add(new Par(1, 1));
                pom.Add(new Par(-q.b, 0));
                rez = rez * new Polinomska(pom);
            }

            Funkcija fizv = (N_izvod(Broj.Count - 1));

            double max;

            max = fizv.Vrednost(Broj[0].b);

            foreach (Par f in Broj)
            {
                double plj = fizv.Vrednost(f.b);
                if (plj >= max)
                    max = plj;
            }
            int fakt = 1;
            for (int i = 1; i <= Broj.Count; i++)
                fakt *= i;
            rez = rez * ((max / fakt) < 0 ? -max / fakt : max / fakt);
            return rez;
        }

        
        //DODATI
        /*public override double greska_int()
        {
            double hoe = h();
            double rez = (MAX() - MIN()) * (hoe * hoe * hoe * hoe) / 180;
            //double rez = (MAX() - MIN()) * (hoe) / 180;
            Funkcija p = N_izvod(4);
            double max = p.Vrednost(Broj[0].b);
            for (int i = 0; i < Broj.Count; i++)
            {
                double pom = p.Vrednost(Broj[i].b);
                if (pom > max) max = pom;
            }
            return rez * max;
        }*/

        public override double greska_int()
        {
            double rez = (MAX() - MIN()) / 12;
            Funkcija p = N_izvod(2);
            double max = Math.Abs(p.fiktivna(Broj[0].b));
            for (int i = 0; i < Broj.Count-2; i++)
            {
                double tren = Broj[i].b;
                if (tren <= MAX() && tren >= MIN())
                {
                    double pom = Math.Abs(p.fiktivna(Broj[i].b));
                    if (pom > max) max = pom;
                }
            }
            return rez * max/(Broj.Count*Broj.Count);
        }



        public override double fiktivna(double x)
        {
            Par l = new Par(), r = new Par();
            foreach (Par w in Broj)
            {
                
                
                l = r;
                r = w;

                if (r.b >= x && l.b <= x) break;

            }
            return l.a+(x-l.b)*(r.a-l.a)/(r.b-l.b);
        }

        public override double OdredjeniIntegral(double a1, double a2)
        {
            if (a1 < Broj[0].b)
                a1 = Broj[0].b;
            if (a2 < Broj[0].b)
                a2 = Broj[0].b;

            if (a1 > Broj[Broj.Count - 1].b)
                a1 = Broj[Broj.Count - 1].b;
            if (a2 > Broj[Broj.Count - 1].b)
                a2 = Broj[Broj.Count - 1].b;
            if (a1 == a2)
                return 0;

            bool obrni = false;
            if (a1 > a2)
            {
                double t = a2;
                a2 = a1;
                a1 = t;
                obrni = true;
            }

            double vrednost = 0;
            double vrednosta1 = 0;
            double vrednosta2 = 0;
            int i = 0;
            for (i = 0; i < Broj.Count - 1; i++)
            {
                if (a1 > Broj[i].b && a1 < Broj[i + 1].b)
                {
                    double osnovica = Broj[i].a + (Broj[i + 1].a - Broj[i].a) / (Broj[i + 1].b - Broj[i].b) * (a1 - Broj[i].b);
                    vrednosta1 = vrednost + (osnovica + Broj[i].a) * (a1 - Broj[i].b) / 2;
                    vrednost += (Broj[i + 1].a + Broj[i].a) * (Broj[i + 1].b - Broj[i].b) / 2;
                }
                else if (a2 > Broj[i].b && a2 < Broj[i + 1].b)
                {
                    double osnovica = Broj[i].a + (Broj[i + 1].a - Broj[i].a) / (Broj[i + 1].b - Broj[i].b) * (a2 - Broj[i].b);
                    vrednost += (a2 - Broj[i].b) / 2 * (Broj[i].a + osnovica);
                    vrednosta2 = vrednost;
                    break;
                }
                else
                {
                    vrednost += (Broj[i + 1].a + Broj[i].a) * (Broj[i + 1].b - Broj[i].b) / 2;
                    if (Broj[i + 1].b == a1)
                        vrednosta1 = vrednost;
                    else if (Broj[i + 1].b >= a2)
                        vrednosta2 = vrednost;
                }
            }

            /*if (Broj[i+1].b > a2 && Broj[i].b < a2)
            {
                double osnovica = Broj[i].a + (Broj[i + 1].a - Broj[i].a) / (Broj[i + 1].b - Broj[i].b) * (a2 - Broj[i].b);
                vrednost += (a2 - Broj[i].b) / 2 * (Broj[i].a + osnovica);
                vrednosta2 = vrednost;
            }*/


            if (obrni)
            {
                return vrednosta1 - vrednosta2;
            }
            return vrednosta2 - vrednosta1;
        }
        public override Funkcija OpstiIntegral(double C = 0)
        {
            List<Par> lista = new List<Par>();
            double vrednost = 0;
            for (int i = 0; i < Broj.Count()-1; i++)
            {
                if (i == 0)
                    vrednost = 0;
                else
                    vrednost += (Broj[i + 1].a + Broj[i].a) * (Broj[i + 1].b - Broj[i].b) / 2;
                lista.Add(new Par(vrednost+C, Broj[i].b));
            }
            return new Tabelarna(lista);
        }

    }
}

/*   class Testiranje
   {
       public static void Main()
       {
           String s = "===============================================================================";
           Polinomska p, p2; ;
           List<Par> pom = new List<Par>(0);
           List<Par> pom2 = new List<Par>(0);
           for (int i = 0; i < 4; i++)
           {
               pom.Add(new Par(i * 2 + 1, i % 2 != 0 ? i : 0));
               pom2.Add(new Par(i, i));
           }

           p = new Polinomska(pom);
           p2 = new Polinomska(pom2);


           Console.WriteLine(p.ToString());
           Console.WriteLine(s);


           Console.WriteLine(p.OpstiIntegral().ToString(1));
           Console.WriteLine(s);

           Console.WriteLine(p.Vrednost(1));
           Console.WriteLine(s);



           Console.Write(p.IntegralUTacki(1));
           Console.Write(" - ");
           Console.Write(p.IntegralUTacki(0));
           Console.Write(" = ");
           Console.WriteLine(p.OdredjeniIntegral(0, 1));

           Console.WriteLine(s);

           List<Par> funkc = new List<Par>(0);

           for (int i = 1; i < 5; i++)
           {
               funkc.Add(new Par(p.Vrednost(i), i));
           }




           Tabelarna t = new Tabelarna(funkc);
           p2 = t.Interpolacioni();

           Console.WriteLine(p.ToString());
           /*for (int i = 1; i < 10; i++)
           {
               Console.Write(i);
               Console.Write(": ");
               Console.Write(p2.Vrednost(i));
               Console.Write(" (");
               Console.Write(p.Vrednost(i));
               Console.WriteLine(")");
           }

           Console.WriteLine(s);
           Console.WriteLine(p2.ToString());
           Console.WriteLine(s);

           Console.WriteLine(p.OdredjeniIntegral(1, 3));
           Console.WriteLine(t.OdredjeniIntegral(1, 3));


           Console.WriteLine(s);
           Console.WriteLine(s);


           Console.ReadKey();
       }
   }

}*/


//using namespace Funkcije___projekat;