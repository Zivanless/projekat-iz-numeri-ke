﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumProj
{
    public class Screen
    {
        private double xs, ys, z, a, xm, ym;
        int k;
        const double defaultzoom = 20;
        public Screen(double xmm, double ymm)
        {
            xs = ys = 0;
            z = defaultzoom;
            k = 0;
            a = 1.2;
            xm = xmm;
            ym = ymm;
        }

        public void translate(double dx, double dy)
        {
            if(xs > 10000000 && dx > 0 || xs < -10000000 && dx < 0 || xs>=-10000000 && xs<=10000000)
                xs = xs - dx / z;
            if (ys > 10000000 && dy < 0 || ys < -10000000 && dy > 0 || ys >= -10000000 && ys <= 10000000)
                ys = ys + dy / z;
        }

        public void zoom(double xmis, double ymis, int al)
        {
            if(al == 0)
                return;
            double zold = z;
            if (al > 0)
            {
                if (z > 1000000)
                    return;
                z = z * a;
                k++; 
            }
            else
            {
                if (1000*z < 1)
                    return;
                z = z / a;
                k--; 
            }
            if (k == 0)
                z = defaultzoom;
            double xt = xs +(2*xmis-xm)/(2*zold);
            double yt = ys - (2 * ymis - ym) / (2 * zold);
            xs = xs * zold / z + xt * (1 - zold / z);
            ys = ys * zold / z + yt * (1 - zold / z);
        }

        public double x_to_screen(double xt) 
        {
            return xm/2 + z*(xt-xs);
        }

        public double y_to_screen(double yt)
        {
            return ym / 2 + z * (ys - yt);
        }

        public double x_from_screen(double xt)
        {
            return xs + (2 * xt - xm) / (2 * z);
        }

        public double y_from_screen(double yt)
        {
            return ys - (2 * yt - ym) / (2 * z);
        }

        public bool in_screen(double x, double y)
        {
            return x >= 0 && x <= xm && y >= 0 && y <= ym;
        }

        public bool in_screen_x(double x)
        {
            return x >= 0 && x <= xm;
        }

        public void reset()
        {
            xs = ys = 0;
            z = defaultzoom;
            k = 0;
        }
        public void resetcenter()
        {
            xs = ys = 0;
        }
        public void resetzoom()
        {
            z = defaultzoom;
            k = 0;
        }
        public double xmax()
        {
            return xm;
        }
        public double ymax()
        {
            return ym;
        }
        public string x_text(double xt)
        {
            double ax = Math.Log10(xm/z)-2;
            if(ax < 0)
                return String.Format("{0}",Math.Round(x_from_screen(xt), 1-(int)ax));
            return String.Format("{0}", Math.Round(x_from_screen(xt), 0));
        }
        public string y_text(double yt)
        {
            double ay = Math.Log10(ym / z) - 2;
            if (ay < 0)
                return String.Format("{0}", Math.Round(y_from_screen(yt), 1 - (int)ay));
            return String.Format("{0}", Math.Round(y_from_screen(yt), 0));
        }
        public string right()
        {
            double ax = Math.Log10(xm/z)-2;
            if(ax < 0)
                return String.Format("{0}",Math.Round(x_from_screen(xm), 1-(int)ax));
            return String.Format("{0}", Math.Round(x_from_screen(xm), 0));    
        }
        public string left()
        {
            double ax = Math.Log10(xm / z)-2;
            if (ax < 0)
                return String.Format("{0}", Math.Round(x_from_screen(0), 1-(int)ax));
            return String.Format("{0}", Math.Round(x_from_screen(0), 0));
        }
        public string down()
        {
            double ay = Math.Log10(ym / z) - 2;
            if (ay < 0)
                return String.Format("{0}", Math.Round(y_from_screen(ym), 1 - (int)ay));
            return String.Format("{0}", Math.Round(y_from_screen(ym), 0));
        }
        public string up()
        {
            double ay = Math.Log10(ym / z) - 2;
            if (ay < 0)
                return String.Format("{0}", Math.Round(y_from_screen(0), 1 - (int)ay));
            return String.Format("{0}", Math.Round(y_from_screen(0), 0));
        }
    }
}
